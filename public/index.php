<?php

declare(strict_types=1);

/*
 * This file is part of Amorid Project
 *
 * (c) Arie W. Subagja
 */

use App\Logger;
use Fig\Http\Message\StatusCodeInterface;
use FrameworkX\AccessLogHandler;
use FrameworkX\App;
use FrameworkX\ErrorHandler;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Message\Response;

require __DIR__.'/../vendor/autoload.php';

$logger = new Logger(__DIR__.'/../var/log.json');
$app = new App(
    $logger->asMiddleware(),
    new AccessLogHandler(),
    new ErrorHandler()
);

$app->get('/', fn () => Response::json([
    'message' => 'Welcome',
    'requests' => $logger->lastLogs(true, 5),
]));

$app->post('/api/notification_apis', fn () => Response::json([
    'message' => 'Received!',
]));

$app->get('/api/organizations', fn () => Response::json([
    'message' => 'Received',
    'organizations' => [],
]));

$app->get('/api/users', fn () => Response::json([
    'message' => 'Received',
    'users' => [],
]));

$app->any('/api/timesheets/daily', function () {
    try {
        $data = file_get_contents(__DIR__.'/../timesheet-daily.response.json');
        $data = json_decode($data, true, 512, \JSON_THROW_ON_ERROR);
    } catch (\Throwable $e) {
        $data = [];
    }

    return Response::json([
        'status' => StatusCodeInterface::STATUS_OK,
        'message' => 'OK',
        'daily_timesheets' => $data,
    ]);
});

$app->any('/api/timesheets', function () {
    try {
        $data = file_get_contents(__DIR__.'/../timesheet.response.json');
        $data = json_decode($data, true, 512, \JSON_THROW_ON_ERROR);
    } catch (\Throwable $e) {
        $data = [];
    }

    return Response::json([
        'status' => StatusCodeInterface::STATUS_OK,
        'message' => 'OK',
        'timesheets' => $data,
    ]);
});

$app->any('/api/timesheets/project', function () {
    try {
        $data = file_get_contents(__DIR__.'/../timesheet.project.response.json');
        $data = json_decode($data, true, 512, \JSON_THROW_ON_ERROR);
    } catch (\Throwable $e) {
        $data = [];
    }

    return Response::json([
        'status' => StatusCodeInterface::STATUS_OK,
        'message' => 'OK',
        'timesheets' => $data,
    ]);
});

$app->post('/api/sign_in', function (ServerRequestInterface $request) {
    try {
        $data = json_decode((string) $request->getBody(), false, 512, \JSON_THROW_ON_ERROR);
    } catch (\Throwable $e) {
        $data = [];
    }

    $username = $data->username ?? null;
    $password = $data->password ?? null;

    $code = StatusCodeInterface::STATUS_OK;
    $message = 'Welcome to Fake JSON API';

    if (!$username || !$password) {
        $code = StatusCodeInterface::STATUS_UNAUTHORIZED;
        $message = 'Missing or invalid credentials';
    }

    try {
        $body = json_encode([
            'code' => $code,
            'message' => $message,
        ], \JSON_THROW_ON_ERROR);
    } catch (\Throwable $e) {
        $body = '';
    }

    return new Response($code, ['Content-Type' => 'application/json'], $body);
});

$app->run();
