<?php

declare(strict_types=1);

/*
 * This file is part of Amorid Project
 *
 * (c) Arie W. Subagja
 */

namespace App;

use Psr\Http\Message\ServerRequestInterface;

class Logger
{
    private string $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;

        $this->init();
    }

    public function asMiddleware(): \Closure
    {
        return function (ServerRequestInterface $request, callable $next) {
            switch (parse_url($request->getRequestTarget(), \PHP_URL_PATH)) {
                case '/api/sign_in':
                    $message = 'api_sign_in';
                    break;
                case '/api/notification_apis':
                    $message = 'api_notification';
                    break;
                case '/api/organizations':
                    $message = 'api_organizations';
                    break;
                case '/api/users':
                    $message = 'api_users';
                    break;
                case '/api/timesheets':
                case '/api/timesheets/daily':
                    $message = 'api_timesheets';
                    break;
                default:
                    $message = 'unknown_path';
                    break;
            }

            try {
                $this->write($message, $request);
            } catch (\Throwable $e) {
            }

            return $next($request);
        };
    }

    /**
     * @throws \JsonException
     */
    public function write(string $message, ServerRequestInterface $request): void
    {
        $data = json_decode((string) $request->getBody(), false, 512, \JSON_THROW_ON_ERROR);

        $context = [
            'method' => $request->getMethod(),
            'uri' => $request->getRequestTarget(),
            'query' => $request->getQueryParams(),
            'data' => $data,
        ];

        $logs = $this->getLogs();

        $logs[] = [
            'date' => date_create_immutable()->format(\DateTimeInterface::ATOM),
            'message' => $message,
            'context' => $context,
        ];

        file_put_contents($this->filePath, json_encode($logs, \JSON_THROW_ON_ERROR));
    }

    public function lastLogs(bool $reverse = true, int $limit = 200): array
    {
        $logs = $this->getLogs($limit);
        $logs = array_map(fn (array $log) => $this->mask($log), $logs);

        if ($reverse) {
            return array_reverse($logs);
        }

        return $logs;
    }

    protected function mask(array $log): array
    {
        $data = $log['context']['data'];

        foreach ($data as $key => $item) {
            if (\in_array($key, ['password', 'username'], true)) {
                $log['context']['data'][$key] = '<redacted>';
            }
        }

        return $log;
    }

    protected function getLogs(int $limit = 200): array
    {
        if ($limit <= 0) {
            $limit = 10;
        }

        if ($limit > 200) {
            $limit = 200;
        }

        $logs = [];

        if (filesize($this->filePath) > 0) {
            try {
                $logInString = file_get_contents($this->filePath);
                $logs = json_decode($logInString, true, 512, \JSON_THROW_ON_ERROR);
            } catch (\Throwable $e) {
                $logs = [];
            }
        }

        return \array_slice($logs, -$limit);
    }

    /**
     * @throws \LogicException
     */
    protected function init(): void
    {
        if (empty($this->filePath)) {
            throw new \LogicException('Filepath could not empty');
        }

        if (!file_exists($this->filePath)) {
            touch($this->filePath);
        }

        if (!is_file($this->filePath)) {
            throw new \LogicException('Filepath is not a file');
        }

        if (!is_writable($this->filePath)) {
            throw new \LogicException('Filepath is not writable');
        }
    }
}
