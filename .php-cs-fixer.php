<?php

declare(strict_types=1);

/*
 * This file is part of Amorid Project
 *
 * (c) Arie W. Subagja
 */

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

$header = <<<HEADER
This file is part of Amorid Project

(c) Arie W. Subagja
HEADER;

$rules = [
    '@Symfony' => true,
    '@Symfony:risky' => true,
    'linebreak_after_opening_tag' => true,
    'mb_str_functions' => true,
    'no_php4_constructor' => true,
    'no_unreachable_default_argument_value' => true,
    'no_useless_else' => true,
    'no_useless_return' => true,
    'php_unit_strict' => true,
    'phpdoc_order' => true,
    'strict_comparison' => true,
    'strict_param' => true,
    'blank_line_between_import_groups' => false,
    'ordered_class_elements' => true,
    'declare_strict_types' => true,
    'header_comment' => ['header' => $header, 'separate' => 'both'],
];

$finder = Finder::create()
    ->in(__DIR__)
    ->exclude('vendor')
    ->ignoreDotFiles(true)
    ->ignoreVCS(true);

return (new Config())
    ->setFinder($finder)
    ->setRules($rules)
    ->setLineEnding("\n")
    ->setRiskyAllowed(true)
    ->setUsingCache(true)
    ->setCacheFile(__DIR__.'/var/.php-cs-fixer.cache')
;
